<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                // Your own constructor code

                $this->load->library('facebook_lib');
        }

	public function login()
	{
		echo $this->facebook_lib->get_login_url();

	}

}
