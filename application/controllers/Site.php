<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	public function index()
	{
		echo format_price(13);
		$data['username'] = 'dara';

		$data['header'] = $this->load->view('header',$data,true);
		$data['content'] = $this->load->view('home_page',$data,true);
		$data['footer'] = $this->load->view('footer',$data,true);
		$this->load->view('index',$data);

	}


	public function detail($title="",$page_id="")
	{
		echo format_price(13);


		$data['page_id'] = $page_id;
		$data['title'] = $title;

		$data['header'] = $this->load->view('header',$data,true);
		$data['content'] = $this->load->view('detail_page',$data,true);
		$data['footer'] = $this->load->view('footer',$data,true);
		$this->load->view('index',$data);

	}


}
