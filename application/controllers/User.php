<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                // Your own constructor code

                $this->load->model('user_model');
        }

	public function index()
	{
		

		$data = array();

		$user_data = array();
		$user_data['full_name'] = $this->input->post('full_name');
		$user_data['sex'] = $this->input->post('sex');
		$user_data['username'] = $this->input->post('username');

		if(count($this->input->post())) {
			$this->user_model->add($user_data);
		}
		
		$data['users'] = $this->user_model->gets();

		$data['header'] = $this->load->view('header',$data,true);
		$data['content'] = $this->load->view('user_page',$data,true);
		$data['footer'] = $this->load->view('footer',$data,true);



		$this->load->view('index',$data);

	}

	public function edit()
	{


		$data = array();

		$user_data = array();
		$user_data['full_name'] = $this->input->post('full_name');
		$user_data['sex'] = $this->input->post('sex');
		$user_data['username'] = $this->input->post('username');

		if(count($this->input->post())) {
			$this->user_model->add($user_data);
		}
		
		$data['users'] = $this->user_model->gets();

		$data['header'] = $this->load->view('header',$data,true);
		$data['content'] = $this->load->view('user_page',$data,true);
		$data['footer'] = $this->load->view('footer',$data,true);



		$this->load->view('index',$data);

	}

	public function detete()
	{

		

		$data = array();

		$user_data = array();
		$user_data['full_name'] = $this->input->post('full_name');
		$user_data['sex'] = $this->input->post('sex');
		$user_data['username'] = $this->input->post('username');

		if(count($this->input->post())) {
			$this->user_model->add($user_data);
		}
		
		$data['users'] = $this->user_model->gets();

		$data['header'] = $this->load->view('header',$data,true);
		$data['content'] = $this->load->view('user_page',$data,true);
		$data['footer'] = $this->load->view('footer',$data,true);



		$this->load->view('index',$data);

	}

}
