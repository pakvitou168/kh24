<?php
class User_model extends CI_Model {

        public $tbl_user = 'user';

        function gets() {
                $query = $this->db->get($this->tbl_user);
                return $query->result();
        }

        function add($data)
        {
                $this->db->insert($this->tbl_user, $data);
        }

}
?>